# Translation of plasma_applet_org.kde.kdeconnect.po into Serbian.
# Slobodan Simić <slsimic@gmail.com>, 2017.
# Chusslove Illich <caslav.ilic@gmx.net>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.kdeconnect\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2019-03-25 07:45+0100\n"
"PO-Revision-Date: 2017-08-03 11:05+0200\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@latin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Generator: Lokalize 1.5\n"

#: package/contents/ui/Battery.qml:40
msgid "%1% charging"
msgstr "%1% puni se"

#: package/contents/ui/Battery.qml:40
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/Battery.qml:40
msgid "No info"
msgstr "nema podataka"

#: package/contents/ui/DeviceDelegate.qml:108
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: package/contents/ui/DeviceDelegate.qml:124
msgid "Ring my phone"
msgstr "zazvoni telefonom"

#: package/contents/ui/DeviceDelegate.qml:142
msgid "Browse this device"
msgstr "pregledaj ovaj uređaj"

#: package/contents/ui/DeviceDelegate.qml:164
msgid "Remote Keyboard"
msgstr "daljinska tastatura"

#: package/contents/ui/DeviceDelegate.qml:202
msgid "Notifications:"
msgstr "Obaveštenja:"

#: package/contents/ui/DeviceDelegate.qml:209
#, fuzzy
#| msgid "Notifications:"
msgid "Dismiss all notifications"
msgstr "Obaveštenja:"

#: package/contents/ui/DeviceDelegate.qml:245
msgid "Reply"
msgstr ""

#: package/contents/ui/DeviceDelegate.qml:254
msgid "Dismiss"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:35
msgid "No paired devices available"
msgstr "nema dostupnih uparenih uređaja"

#: package/contents/ui/main.qml:59
msgid "KDE Connect Settings..."
msgstr "Postavke za KDE‑konekciju..."