# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-urlhandler\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 07:36+0200\n"
"PO-Revision-Date: 2019-04-01 15:01+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: Gonzalez Pol Connect Aleix\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "José Nuno Pires"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "zepires@gmail.com"

#: kdeconnect-handler.cpp:56
#, kde-format
msgid "KDE Connect URL handler"
msgstr "Tratamento de URL's do KDE Connect"

#: kdeconnect-handler.cpp:62
#, kde-format
msgid "(C) 2017 Aleix Pol Gonzalez"
msgstr "(C) 2017 Aleix Pol Gonzalez"

#: kdeconnect-handler.cpp:69
#, kde-format
msgid "URL to share"
msgstr "URL a partilhar"

#: kdeconnect-handler.cpp:94
#, kde-format
msgid "Device to call this phone number with:"
msgstr "O dispositivo com o qual contactar este número de telefone:"

#: kdeconnect-handler.cpp:111
#, kde-format
msgid "Couldn't share %1"
msgstr "Não foi possível partilhar o %1"