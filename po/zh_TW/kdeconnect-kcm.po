# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# liueigi <liueigi@gmail.com>, 2016.
# Jeff Huang <s8321414@gmail.com>, 2016.
# pan93412 <pan93412@gmail.com>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kcm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 07:36+0200\n"
"PO-Revision-Date: 2018-12-08 11:43+0800\n"
"Last-Translator: pan93412 <pan93412@gmail.com>\n"
"Language-Team: Chinese <zh-l10n@linux.org.tw>\n"
"Language: Traditional Chinese\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "liueigi, Jeff Huang"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "liueigi@gmail.com, s8321414@gmail.com"

#: kcm.cpp:57
#, kde-format
msgid "KDE Connect Settings"
msgstr "KDE連線設定"

#: kcm.cpp:59
#, kde-format
msgid "KDE Connect Settings module"
msgstr "KDE連線設定模組"

#: kcm.cpp:61
#, kde-format
msgid "(C) 2015 Albert Vaca Cintora"
msgstr "(C) 2015 Albert Vaca Cintora"

#: kcm.cpp:65
#, kde-format
msgid "Albert Vaca Cintora"
msgstr "Albert Vaca Cintora"

#: kcm.cpp:249
#, kde-format
msgid "Available plugins"
msgstr "可以用的擴充插件"

#: kcm.cpp:302
#, kde-format
msgid "Error trying to pair: %1"
msgstr "嘗試 %1 配對錯誤"

#: kcm.cpp:323
#, kde-format
msgid "(paired)"
msgstr "（已配對）"

#: kcm.cpp:326
#, kde-format
msgid "(not paired)"
msgstr "（未配對）"

#: kcm.cpp:329
#, kde-format
msgid "(incoming pair request)"
msgstr "（到來的配對請求）"

#: kcm.cpp:332
#, kde-format
msgid "(pairing requested)"
msgstr "（已請求配對）"

#. i18n: ectx: property (text), widget (QLabel, rename_label)
#: kcm.ui:47
#, kde-format
msgid "KDE Connect"
msgstr "KDE連線"

#. i18n: ectx: property (text), widget (QToolButton, renameShow_button)
#: kcm.ui:70
#, kde-format
msgid "Edit"
msgstr "編輯"

#. i18n: ectx: property (text), widget (QToolButton, renameDone_button)
#: kcm.ui:92
#, kde-format
msgid "Save"
msgstr "儲存"

#. i18n: ectx: property (text), widget (QPushButton, refresh_button)
#: kcm.ui:108
#, kde-format
msgid "Refresh"
msgstr "刷新"

#. i18n: ectx: property (text), widget (QLabel, name_label)
#: kcm.ui:155
#, kde-format
msgid "Device"
msgstr "裝置"

#. i18n: ectx: property (text), widget (QLabel, status_label)
#: kcm.ui:171
#, kde-format
msgid "(status)"
msgstr "（狀態）"

#. i18n: ectx: property (text), widget (QPushButton, accept_button)
#: kcm.ui:210
#, kde-format
msgid "Accept"
msgstr "同意"

#. i18n: ectx: property (text), widget (QPushButton, reject_button)
#: kcm.ui:217
#, kde-format
msgid "Reject"
msgstr "拒絕"

#. i18n: ectx: property (text), widget (QPushButton, pair_button)
#: kcm.ui:230
#, kde-format
msgid "Request pair"
msgstr "請求配對"

#. i18n: ectx: property (text), widget (QPushButton, unpair_button)
#: kcm.ui:243
#, kde-format
msgid "Unpair"
msgstr "取消配對"

#. i18n: ectx: property (text), widget (QPushButton, ping_button)
#: kcm.ui:256
#, kde-format
msgid "Send ping"
msgstr "傳送Ping回應封包"

#. i18n: ectx: property (text), widget (QLabel, noDeviceLinks)
#: kcm.ui:294
#, kde-format
msgid ""
"<html><head/><body><p>No device selected.<br><br>If you own an Android "
"device, make sure to install the <a href=\"https://play.google.com/store/"
"apps/details?id=org.kde.kdeconnect_tp\"><span style=\" text-decoration: "
"underline; color:#4c6b8a;\">KDE Connect Android app</span></a> (also "
"available <a href=\"https://f-droid.org/repository/browse/?fdid=org.kde."
"kdeconnect_tp\"><span style=\" text-decoration: underline; color:#4c6b8a;"
"\">from F-Droid</span></a>) and it should appear in the list.<br><br>If you "
"are having problems, visit the <a href=\"https://community.kde.org/KDEConnect"
"\"><span style=\" text-decoration: underline; color:#4c6b8a;\">KDE Connect "
"Community wiki</span></a> for help.</p></body></html>"
msgstr ""
"<html><head/><body><p>沒有裝置被選擇</p><br/><br/>假如您擁有一台 Android 裝"
"置，請確定您有安裝 <a href=\"https://play.google.com/store/ apps/details?"
"id=org.kde.kdeconnect_tp\"><span style=\" text-decoration: underline; color:"
"#4c6b8a;\">KDE Connect（KDE連線） Android app</span></a>（同時也在 <a href="
"\"https://f-droid.org/repository/browse/?fdid=org.kde. kdeconnect_tp\"><span "
"style=\" text-decoration: underline; color:#0000ff;\">F-Droid 上提供</span></"
"a>）那麼它就會在列表當中出現。<br/><br/>如果您還有其他問題，請參閱 <a href="
"\"https://community.kde.org/KDEConnect \"><span style=\" text-decoration: "
"underline; color:#4c6b8a;\">KDE 連線社群維基</span></a>尋求協助。</p></"
"body></html>"