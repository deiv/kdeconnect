# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Alexander Potashev <aspotashev@gmail.com>, 2015, 2017.
# Dmitry Serov <dmitri.s93@gmail.com>, 2015.
# Alexander Yavorsky <kekcuha@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 07:36+0200\n"
"PO-Revision-Date: 2019-07-22 20:33+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.3\n"

#: backends/bluetooth/bluetoothpairinghandler.cpp:70
#: backends/lan/lanpairinghandler.cpp:66
#, kde-format
msgid "Canceled by other peer"
msgstr "Отменено другим клиентом."

#: backends/bluetooth/bluetoothpairinghandler.cpp:79
#: backends/lan/lanpairinghandler.cpp:75
#, kde-format
msgid "%1: Already paired"
msgstr "%1: Устройство уже сопряжено"

#: backends/bluetooth/bluetoothpairinghandler.cpp:82
#, kde-format
msgid "%1: Pairing already requested for this device"
msgstr "%1: Сопряжение уже запрошено для этого устройства"

#: backends/bluetooth/bluetoothpairinghandler.cpp:138
#: backends/lan/lanpairinghandler.cpp:119
#, kde-format
msgid "Timed out"
msgstr "Истекло время ожидания."

#: backends/lan/landevicelink.cpp:144 backends/lan/landevicelink.cpp:158
#, kde-format
msgid ""
"This device cannot be paired because it is running an old version of KDE "
"Connect."
msgstr ""
"Это устройство не может быть сопряжено, так как на нём установлена старая "
"версия KDE Connect."

#: backends/lan/uploadjob.cpp:50
#, kde-format
msgid "Couldn't find an available port"
msgstr "Не удалось найти доступный порт"

#: device.cpp:156
#, kde-format
msgid "Already paired"
msgstr "Устройство уже сопряжено."

#: device.cpp:161
#, kde-format
msgid "Device not reachable"
msgstr "Устройство недоступно."

#: device.cpp:473
#, kde-format
msgid "SHA1 fingerprint of your device certificate is: %1\n"
msgstr "Отпечаток SHA-1 сертификата вашего устройства: %1\n"

#: device.cpp:481
#, kde-format
msgid "SHA1 fingerprint of remote device certificate is: %1\n"
msgstr "Отпечаток SHA-1 сертификата удалённого устройства: %1\n"

# BUGME: please change to "KDE Connect" --aspotashev
#: filetransferjob.cpp:61 filetransferjob.cpp:84
#, kde-format
msgid "Receiving file over KDE Connect"
msgstr "Получение файла через KDE Connect"

#: filetransferjob.cpp:62 filetransferjob.cpp:85
#, kde-format
msgctxt "File transfer origin"
msgid "From"
msgstr "С устройства"

#: filetransferjob.cpp:67
#, kde-format
msgid "Filename already present"
msgstr "Файл с таким именем уже существует."

#: filetransferjob.cpp:86
#, kde-format
msgctxt "File transfer destination"
msgid "To"
msgstr "В"

#: filetransferjob.cpp:114
#, kde-format
msgid "Received incomplete file: %1"
msgstr "Получен неполный файл: %1"

#: kdeconnectconfig.cpp:69
#, kde-format
msgid "KDE Connect failed to start"
msgstr "Не удалось запустить KDE Connect."

#: kdeconnectconfig.cpp:70
#, kde-format
msgid ""
"Could not find support for RSA in your QCA installation. If your "
"distribution provides separate packets for QCA-ossl and QCA-gnupg, make sure "
"you have them installed and try again."
msgstr ""
"На вашей системе установлен пакет QCA, но в нём отсутствует поддержка RSA. "
"Если ваш дистрибутив содержит отдельные пакеты для QCA-ossl и QCA-gnupg, "
"установите их и повторите попытку."

#: kdeconnectconfig.cpp:96
#, kde-format
msgid "Could not store private key file: %1"
msgstr "Не удалось сохранить файл закрытого ключа: %1"

#: kdeconnectconfig.cpp:136
#, kde-format
msgid "Could not store certificate file: %1"
msgstr "Не удалось сохранить файл сертификата: %1"