# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2015, 2016, 2017, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 07:36+0200\n"
"PO-Revision-Date: 2019-01-03 10:15+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: package/contents/ui/Battery.qml:40
msgid "%1% charging"
msgstr "%1% opladen"

#: package/contents/ui/Battery.qml:40
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/Battery.qml:40
msgid "No info"
msgstr "Geen informatie"

#: package/contents/ui/DeviceDelegate.qml:108
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: package/contents/ui/DeviceDelegate.qml:124
msgid "Ring my phone"
msgstr "Bel mijn telefoon"

#: package/contents/ui/DeviceDelegate.qml:142
msgid "Browse this device"
msgstr "Dit apparaat langslopen"

#: package/contents/ui/DeviceDelegate.qml:164
msgid "Remote Keyboard"
msgstr "Toetsenbord op afstand"

#: package/contents/ui/DeviceDelegate.qml:202
msgid "Notifications:"
msgstr "Meldingen:"

#: package/contents/ui/DeviceDelegate.qml:209
msgid "Dismiss all notifications"
msgstr "Alle meldingen wegdoen"

#: package/contents/ui/DeviceDelegate.qml:245
msgid "Reply"
msgstr "Beantwoorden"

#: package/contents/ui/DeviceDelegate.qml:254
msgid "Dismiss"
msgstr "Wegdoen"

#: package/contents/ui/FullRepresentation.qml:35
msgid "No paired devices available"
msgstr "Geen gepaarde apparaten beschikbaar"

#: package/contents/ui/main.qml:59
msgid "KDE Connect Settings..."
msgstr "Instellingen van KDE Connect..."